# CONCERTOACUSMATICO

Questo collettivo si riunisce con la volontà di esplorare le possibilità compositive nella forma di brani acusmatici. 

Il pensiero si sofferma sulla decostruzione di spazializzazione e fascia sonora.



## Spazializzazione

Innanzitutto è necessario delineare cosa intendiamo per spazializzazione, spiegando qual è la tecnica o meglio lo -strumento- di cui ci serviamo per pensare.
Lo strumento utilizzato prende vita dagli articoli di Gerzon e dal brevetto di Blumlein del 1933 sulla possibilità di catturare e diffondere un ascolto stereofonico. 
Di qui bisogna delineare cosa si intende per stereofonico, partendo dall'etimologia: "Stereo" Solido. Il senso di solidità di un suono inserito in uno spazio che garantisce una localizzazione, una profondità e una distanza della sorgente rispetto al punto di ascolto.
La denotazione quindi di una sorgente che genera un campo acustico che svela la natura dello spazio. 
Alla definizione di stereofonia è legato anche il nostro ascolto da un punto di vista percettivo: l'ascolto è solido dal momento in cui sono presenti due punti (le orecchie) che catturano l'informazione circostante. La differenza di informazione in fase e ampiezza delinea e connota la sorgente in relazione allo spazio. 

Tornando alla natur dello strumento, e ricordando che la forma del brano è acusmatica, potremmo servirci sia di materiale concreto rielaborato per un approccio acustico o materiale di sintesi. Il lettore attento dopo questa precisazione si domanderebbe: Va bene per del mteriale concreto quindi catturato all'interno di un panorama stereofonico con una tecnica stereofonica, ma il materiale catturato con una tecnica che non soddisfa le differenze di fase e di ampiezza per essere definita stereofonica come si pone nei confronti di questo approccio? O un suono di sintesi che per sua natura non vive di uno spazio?
E' qui che entra in gioco lo pseudo-stereo, ovvero un algoritmo che simula la cattura stereofonica prendendo in ingresso un segnale mono e dà in uscita due segnali: il MID (il segnale che descrive la natura frontale del suono, ovvero il segnale che mantiene le informazioni che hanno interferenze costruttive) e il SIDE (il segnale che descrive le differenze). 
Potremmo figurarci il mid come il suono che agisce lungo l'asse delle ordinate e il side come quello che agisce sull'asse delle ascisse.
In definitiva possiamo anche considerare il mid come il segnale diretto e il side come il segnale laterale, le riflessioni del suono.

Ora abbiamo due segnali che ci descrivono un suono, differentemente dai segnali che finiranno in uscita e useremo per ascoltare il suono in maniera sterofonica.

## Fascia Sonora

La fascia sonora è un concetto dominante nelle riflessioni e nei pensieri della musica elettronica, in particolare nella Scuola Romana. 

Il nostro lavoro si occupa di decostruire il concetto di fascia sonora e utilizzarlo per aprire a un pensiero proprio. 
