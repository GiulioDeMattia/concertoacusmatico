import("stdfaust.lib");
// PSEUDO-STEREO ENCODER. FROM MONO TO MS
declare author "SMERM";


//---		SLIDERS
pp= hslider("Polar Pattern",50,0,100,1)/100:si.smoo;
rad=(0-hslider("Degree",0,-180,180,1))*(ma.PI)/180:si.smoo;

//---	MS FUNCTION
mspan(pp,rad,x)= mid(pp,rad,x),side(rad,x)
with{
mid(pp,rad,x)=(x*(1-pp)) + (x*cos(rad)*pp);
side(rad,x)=x*sin(rad);
};
vstin= _, !;

//---	PROCESS
process=vstin:mspan(pp,rad);
