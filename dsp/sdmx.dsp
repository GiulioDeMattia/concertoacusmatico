import("stdfaust.lib");
// --- SUM-DIFFERENCE MATRIX --- 
// allows you to switch between LR and MS
// -----------------------------
sums=+/sqrt(2);
difs=-/sqrt(2);
sdmx = sums,difs;
process=si.bus(2)<:sdmx;

