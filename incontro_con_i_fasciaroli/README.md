# Concerto acusmatico sulle fasce

## Definizione di fascia sonora
  - maschera di tendenza
    - range frequenziale
    - materiale con sintesi additiva
    - nuvola di grani
    - non qualcosa di riconducibile ai suoni forma?
    - discorso distribuito su un lasso di tempo più lungo
  - movimenti di parametri e fissazione di parametri
    - attenzione alla fase per la decodifica
## Definizione dei Luoghi
  - [Zalib a Trastevere](https://www.libreriazalib.com/)
  - [Cosmo a Trastevere](https://zero.eu/it/luoghi/240765-cosmo,roma/)
  - [Jey a Ostiense](https://www.facebook.com/jazzlivemusic/)
## Durata dei brani (7/8 minuti)
  - organizzazione del materiale stesa su più minuti è un'altro punto di ricerca
  - orgnaizzare 8 minuti in un brano per avere
## Brani di letteratura fondamentali
  - [Truax - Riverrun](https://www.youtube.com/watch?si=hzHDEIs4eeQUG_GN&v=u81IGEFt7dM&feature=youtu.be&ab_channel=SebastianArsAcoustica)
  - [Smalley - Valley Flow](https://www.youtube.com/watch?si=4jKcYyoNoN5LdAMq&v=9MLMxXPQ-C4&feature=youtu.be&ab_channel=SebastianArsAcoustica)
  - [Risset - Mutations](https://www.youtube.com/watch?si=YABHmotdd7jqJwW-&v=u83PZ1NN34s&feature=youtu.be&ab_channel=monotoniac)
  - [Wessel - Antony](https://www.youtube.com/watch?si=8RyjkdaTWuUhUUhn&v=MfxmMjlU1wE&feature=youtu.be)
  - [Haas - Concerto per Trombone e Orchestra](inserire link)
  - [Murail - Tellur](brano per orchestra)
  - [Romitelli - ](brano per orchestra)
  - [Ligeti - Lux Aeterna]()
## Definizione del concerto
  - il concerto non deve assumere un'aura solenne
  - si deve poter camminare o sedersi
## Definizione dei brani e dello spazio
  - scrivere all'interno di una sfera

## Date
- 12 ottobre 2023 -> prima idea di fascia
- 29 ottobre 2023 -> prima consegna di idea compositiva del brano
- 10 novembre 2023 -> consegna del brano completo
